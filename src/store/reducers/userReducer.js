import {USER_LOGIN} from "../../constants/actions";

export default function (state = {}, action) {
  switch(action.type) {
    case USER_LOGIN:
      return {...state, auth: action.payload};

    default: return state;
  }
}