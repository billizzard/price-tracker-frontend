import {combineReducers} from "redux";
import user from './userReducer'
import snackbar from './snackbarReducer'
import {reducer as formReducer} from 'redux-form'

const rootReducer = combineReducers({
  form: formReducer,
  user,
  snackbar,
});

export default rootReducer