import axios from 'axios';

export const loginUserRequest = (username, password) => {
  return axios.post('/api/auth/login', {username, password});
};
