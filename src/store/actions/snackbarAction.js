import {CLOSE_SNACKBAR, OPEN_SNACKBAR} from "../../constants/actions";

export const closeSnackbarAction = (previousMessageType = '') => async dispatch => {
    dispatch({
      type: CLOSE_SNACKBAR,
      payload: {
        isOpen: false,
        message: '',
        type: previousMessageType,
      }
    })
};

export const openSnackbarAction = (message, type) => async dispatch => {
    dispatch({
        type: OPEN_SNACKBAR,
        payload: {
            isOpen: true,
            message: message,
            type: type,
        }
    })
};