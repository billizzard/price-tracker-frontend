import {USER_LOGIN} from "../../constants/actions";
import {loginUserRequest} from "../requests/userRequest";

export const loginUser = (username, password) => async dispatch => {
  const response = await loginUserRequest(username, password);

  if (response) {
    dispatch({
      type: USER_LOGIN,
      payload: response.data
    })
  }
};