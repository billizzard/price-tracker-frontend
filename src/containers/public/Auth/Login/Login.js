import React from 'react';
import FrontLayout from "../../../../hoc/layout/FrontendLayout";
import AuthForm from "../../../../components/forms/AuthForm/AuthForm";
import {connect} from "react-redux";
import {loginUser} from "../../../../store/actions";

class Login extends React.Component {
    submitLoginForm = values => {
        this.props.dispatch(loginUser(
            values.login,
            values.password
        ));
    };

    submitRegistrationForm = values => {
        this.props.dispatch(loginUser(
            values.login,
            values.password
        ));
    };

    render() {
        return (
            <AuthForm submitLoginForm={this.submitLoginForm} submitRegistrationForm={this.submitRegistrationForm}/>
        )
    }
}

export default FrontLayout(connect(null, null)(Login))
