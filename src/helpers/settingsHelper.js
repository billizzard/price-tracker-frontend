import {OPEN_SNACKBAR} from "../constants/actions";
import {MESSAGE_TYPE_ERROR} from "../constants/common";

export const setAxiosSettings = (axios, dispatch) => {
  axios.defaults.baseURL = process.env.BASE_URL;
  axios.defaults.headers.common['Authorization'] = '';
  axios.defaults.headers.post['Content-Type'] = 'application/json';

  axios.interceptors.request.use(request => {
    return request;
  }, error => {
    return Promise.reject(error);
  });

  axios.interceptors.response.use(response => {
    return response;
  }, error => {
    dispatch({
      type: OPEN_SNACKBAR,
      payload: {
        messageType: MESSAGE_TYPE_ERROR,
        message: error.message,
        isOpen: true,
      }
    });

    return Promise.reject(error);
  });
};


