import React from 'react';
// import classes from './FixedNavBar.css'
import LeftNavBarMenu from "./LeftNavBarMenu/LeftNavBarMenu";
import RightNavBarMenu from "./RightNavBarMenu/RightNavBarMenu";
import {IMenuItem} from "../../typescript/interfaces";

interface FixedNavBarProps {
    leftMenuItems: Array<IMenuItem>;
    rightMenuItems: Array<IMenuItem>;
}

function FixedNavBar(props: FixedNavBarProps) {
    const {leftMenuItems, rightMenuItems} = props;

    return (
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a className="navbar-brand" href="/">Fixed navbar</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarCollapse">
                <LeftNavBarMenu menuItems={leftMenuItems}/>
                <RightNavBarMenu menuItems={rightMenuItems}/>
            </div>
        </nav>
    );
}

export default FixedNavBar;
