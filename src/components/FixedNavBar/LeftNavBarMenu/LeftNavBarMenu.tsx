import React from 'react';
import MenuItem from "./MenuItem/MenuItem";
import {IMenuItem} from "../../../typescript/interfaces";

interface LeftNavBarMenuProps {
    menuItems: Array<IMenuItem>;
}

function LeftNavBarMenu(props: LeftNavBarMenuProps) {
    const renderMenuItems = () => {
        if (props.menuItems) {
            return props.menuItems.map((item) => {
                return (
                    <MenuItem key={item.id} {...item}/>
                )
            })
        }
    }

    return (
        <ul className="navbar-nav mr-auto">
            {renderMenuItems()}
        </ul>
    );
}

export default LeftNavBarMenu;
