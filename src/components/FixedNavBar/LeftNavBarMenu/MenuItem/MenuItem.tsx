import React from 'react';
import {IMenuItem} from "../../../../typescript/interfaces";
import {Link} from 'react-router-dom'

function MenuItem(props: IMenuItem) {
    return (
        <li className={'nav-item ' + (props.isActive ? 'active' : '')}>
            <Link className={'nav-link ' + (props.isDisabled ? 'disabled' : '')} to={props.link}>{props.name}</Link>
        </li>
    );
}

export default MenuItem;
