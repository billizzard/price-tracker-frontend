import React from 'react';
import {IMenuItem} from "../../../typescript/interfaces";
import {Link} from 'react-router-dom';

interface Props {
    menuItems: Array<IMenuItem>;
}

function RightNavBarMenu(props: Props) {
    const renderMenuItems = () => {
        if (props.menuItems) {
            return props.menuItems.map((item) => {
                return (
                    <Link key={item.id} className="btn btn-outline-success my-2 my-sm-0" to={item.link}>{item.name}</Link>
                )
            })
        }
    };

    return (
        <div>
            {renderMenuItems()}
        </div>
    );
}

export default RightNavBarMenu;
