import React, {useState} from 'react';
import classes from './AuthForm.module.css'
import LoginForm from "./LoginForm/LoginForm";
import RegistrationForm from "./RegistrationForm/RegistrationForm";

interface Props {
    submitLoginForm: () => void;
    submitRegistrationForm: () => void;
}

function AuthForm(props: Props) {
    const [formType, setFormType] = useState({
        signIn: true,
        signUp: false
    });

    const switchForm = (isShowLoginForm) => () => {
        setFormType({
            signIn: isShowLoginForm,
            signUp: !isShowLoginForm,
        });
    };

    return (
        <div className={classes.LoginWrapper}>
            <div className={classes.Login}>
                <label className={classes.tab  + ' ' + (formType.signIn ? classes.active : '')} onClick={switchForm(true)}>
                    Sign In
                </label>

                <label className={classes.tab  + ' ' + (formType.signUp ? classes.active : '')} onClick={switchForm(false)}>
                    Sign Up
                </label>

                <div className={classes["login-form"]}>
                    <div className={classes["sign-in-htm"]  + ' ' + (formType.signIn ? classes.show : '')}>
                        <LoginForm onSubmit={props.submitLoginForm}/>
                    </div>

                    <div className={classes["sign-up-htm"]  + ' ' + (formType.signUp ? classes.show : '')}>
                        <RegistrationForm onSubmit={props.submitRegistrationForm}/>
                        <div className={classes.hr}/>
                        <div className={classes["foot-lnk"]}>
                            <label onClick={switchForm(true)}>Already Member?</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AuthForm;
