import React from 'react';
import classes from '../AuthForm.module.css'


function RegistrationForm(props) {
    return (
        <form>
            <div className={classes.group}>
                <label htmlFor="user" className={classes.label}>Username</label>
                <input id="user" type="text" className={classes.input}/>
            </div>
            <div className={classes.group}>
                <label htmlFor="pass" className={classes.label}>Password</label>
                <input id="pass" type="password" className={classes.input} data-type="password"/>
            </div>
            <div className={classes.group}>
                <label htmlFor="pass" className={classes.label}>Repeat Password</label>
                <input id="pass" type="password" className={classes.input} data-type="password"/>
            </div>
            <div className={classes.group}>
                <label htmlFor="pass" className={classes.label}>Email Address</label>
                <input id="pass" type="text" className={classes.input}/>
            </div>
            <div className={classes.group}>
                <input type="submit" className={classes.button} value="Sign Up"/>
            </div>
        </form>
    );
}

export default RegistrationForm;
