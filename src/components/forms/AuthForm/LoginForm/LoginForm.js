import React from 'react';
import classes from '../AuthForm.module.css'
import {reduxForm} from "redux-form";

let LoginForm = props => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div className={classes.group}>
                <label htmlFor="user" className={classes.label}>Username</label>
                <input id="user" type="text" className={classes.input}/>
            </div>
            <div className={classes.group}>
                <label htmlFor="pass" className={classes.label}>Password</label>
                <input id="pass" type="password" className={classes.input} data-type="password"/>
            </div>
            <div className={classes.group}>
                <input id="check" type="checkbox" className={classes.check} checked/>
                <label htmlFor="check"><span className={classes.icon}/> Keep me Signed in</label>
            </div>
            <div className={classes.group}>
                <input type="submit" className={classes.button} value="Sign In"/>
            </div>

            <div className={classes.hr}/>
            <div className={classes["foot-lnk"]}>
                <a href="#forgot">Forgot Password?</a>
            </div>
        </form>
    );
}

LoginForm = reduxForm({
    form: 'loginForm',
})(LoginForm);

export default LoginForm;
