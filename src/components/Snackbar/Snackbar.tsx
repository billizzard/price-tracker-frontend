import React from 'react';
import styles from './Snackbar.module.css';

interface Props {
    message: string;
    type: string;
    isOpen: boolean;
    onClose: () => any;
}

function Snackbar(props: Props) {
    return (
        <div className={styles.Snackbar + " alert alert-" + props.type + " alert-dismissible fade " + (props.isOpen ? 'show' : 'hide')} role="alert">
            {props.message}
            <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={props.onClose}>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    );
}

export default Snackbar;
