export const MESSAGE_TYPE_ERROR = 'danger';
export const MESSAGE_TYPE_SUCCESS = 'success';
export const MESSAGE_TYPE_PRIMARY = 'primary';