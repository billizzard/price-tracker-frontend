export const leftTopMenuItems = [
    {id: 1, name: 'Home', link: '/'},
    {id: 2, name: 'Text', link: '/test', isDisabled: true},
];

export const rightTopMenuItems = [
    {id: 3, name: 'Login', link: '/auth/login'},
];