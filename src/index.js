import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import thunk from 'redux-thunk'
import Routes from './routes'
import axios from 'axios'

// COMPONENTS
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from "redux";
import reducers from './store/reducers'
import {composeWithDevTools} from "redux-devtools-extension";
import {setAxiosSettings} from "./helpers/settingsHelper";

const composeEnhancers = composeWithDevTools({});
const store = createStore(reducers, composeEnhancers(
    applyMiddleware(thunk),
));

setAxiosSettings(axios, store.dispatch);

const App = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Routes/>
            </BrowserRouter>
        </Provider>
    )
}

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
)