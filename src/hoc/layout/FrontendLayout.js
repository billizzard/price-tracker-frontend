import React from 'react';
import FixedNavBar from "../../components/FixedNavBar/FixedNavBar";
import {leftTopMenuItems, rightTopMenuItems} from '../../constants/menus'
import Snackbar from "../../components/Snackbar/Snackbar";
import {closeSnackbarAction} from "../../store/actions";
import {connect} from 'react-redux'

// // COMPONENTS
// import Header from '../components/layouts/Header'
// import Snackbar from '../components/popups/snackbar/GlobalSnackbar'
// import LoadingOverlay from "../components/layouts/LoadingOverlay";


const FrontLayout = (WrappedElement) => {
    class HOC extends React.Component {
        closeSnackbar = () => {
            this.props.dispatch(closeSnackbarAction())
        };

        render() {
            return (
                <div>
                    <FixedNavBar leftMenuItems={leftTopMenuItems} rightMenuItems={rightTopMenuItems}/>
                    <div className='page'>
                        <main>
                            <WrappedElement/>
                            <Snackbar {...this.props.snackbar} onClose={this.closeSnackbar}/>
                        </main>
                    </div>
                    {/*<LoadingOverlay*/}
                        {/*active={this.props.isPageLoading}*/}
                    {/*>*/}
                        {/*<Header/>*/}
                        {/*<WrappedPage {...this.props}/>*/}
                        {/*<Snackbar />*/}
                    {/*</LoadingOverlay>*/}
                </div>
            )
        }
    }

    function mapStateToProps(state) {
        return {
            snackbar: state.snackbar.data,
        }
    }

    return connect(mapStateToProps, null)(HOC);
};

export default FrontLayout;
