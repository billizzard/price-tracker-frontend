import React, {Component} from 'react';
import { Route, Switch} from 'react-router-dom';
import Home from "./containers/public/Home/Home";
import NotFound from "./containers/public/NotFound/NotFound";
import Login from "./containers/public/Auth/Login/Login";

class Routes extends Component {
    render() {
        return(
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/auth/login" exact component={Login}/>
                <Route component={NotFound}/>
            </Switch>
        )
    }
}

export default Routes;