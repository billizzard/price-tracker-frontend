export interface IMenuItem {
    id: number,
    name: string,
    link: string,
    isActive?: boolean,
    isDisabled?: boolean,
}